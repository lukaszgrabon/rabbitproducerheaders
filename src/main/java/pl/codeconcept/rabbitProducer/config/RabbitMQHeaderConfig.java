package pl.codeconcept.rabbitProducer.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQHeaderConfig {

    @Bean
    Queue teksas1Queue() {
        return new Queue("teksas1Queue", false);
    }

    @Bean
    Queue teksas2Queue() {
        return new Queue("teksas2Queue", false);
    }

    @Bean
    Queue teksas3Queue() {
        return new Queue("teksas3Queue", false);
    }

    @Bean
    HeadersExchange headerExchange() {
        return new HeadersExchange("header-exchange");
    }

    @Bean
    Binding teksas1Binding(Queue teksas1Queue, HeadersExchange headerExchange) {
        return BindingBuilder.bind(teksas1Queue).to(headerExchange).where("department").matches("teksas1");
    }

    @Bean
    Binding teksas2Binding(Queue teksas2Queue, HeadersExchange headerExchange) {
        return BindingBuilder.bind(teksas2Queue).to(headerExchange).where("department").matches("teksas2");
    }

    @Bean
    Binding teksas3Binding(Queue teksas3Queue, HeadersExchange headerExchange) {
        return BindingBuilder.bind(teksas3Queue).to(headerExchange).where("department").matches("teksas3");
    }
}
